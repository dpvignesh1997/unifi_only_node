<?php

class ControllerApiAllproducts extends Controller {
    private $error = array();

   public function index()
   {
        $products = array();
        $this->load->language('catalog/product');
        $this->load->model('catalog/product');
        $this->load->model('api/product');
        $this->load->model('tool/image');

        $error[]['no_json']= "No JSON";

        // preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $matches);
       //    $last_word = $matches[0];
        $product_total = $this->model_catalog_product->getTotalProducts();

        $results = $this->model_catalog_product->getProducts();

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], 40, 40);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 40, 40);
            }

            $special = false;

            $product_specials = $this->model_api_product->getProductSpecials($result['product_id']);

            foreach ($product_specials  as $product_special) {
                if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
                    $special = $product_special['price'];

                    break;
                }
            }

            $shop_products['shop_products'][] = array(
                'product_id' => $result['product_id'],
                'image'      => $image,
                'name'       => $result['name'],
                'model'      => $result['model'],
                'price'      => $result['price'],
                'special'    => $special,
                'quantity'   => $result['quantity'],
                'status'     => $result['status']
            );
        }


        if (isset($this->request->get['json'])) {
            echo json_encode($shop_products);
            die;
        } else {
            $this->response->setOutput(json_encode($error));
        }  
   }

   public function products()
   {

    $this->load->language('catalog/product');
    $this->load->model('catalog/product');
    $this->load->model('tool/image');

     $product_details = array();
     $error['fail'] = 'Failed';
    if (isset($this->request->get['category'])) {
         //$product_details['product_id'] = $this->request->get['product_id'];
        $products = $this->model_catalog_product->getProducts(array(
			'filter_category_id'	=> $this->request->get['category']
        ));
        
        foreach ($products as $product) {

			if (is_file(DIR_IMAGE . $product['image'])) {
                $image = $this->model_tool_image->resize($product['image'], 500, 500);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 500, 500);
            }

			$json['product'][] = array(
                'id'                            => $product['product_id'],
                'name'                          => $product['name'],
                'model'                         => $product['model'],
                'image'                         => $image,
                'price'                         => $product['price'],
                'description'                   => $product['description'],
                'attribute_groups'              => $this->model_catalog_product->getProductAttributes($product['product_id'])
            );
		}

            echo json_encode($json);
            die;
        } else {
            $this->response->setOutput(json_encode($error));
        }
   }

   public function product()
   {

    $this->load->language('catalog/product');
    $this->load->model('catalog/product');
    $this->load->model('tool/image');

     $product_details = array();
     $error['fail'] = 'Failed';
    if (isset($this->request->get['product_id'])) {
         //$product_details['product_id'] = $this->request->get['product_id'];
         $product_details = $this->model_catalog_product->getProduct($this->request->get['product_id']);


			if (is_file(DIR_IMAGE . $product_details['image'])) {
                $image = $this->model_tool_image->resize($product_details['image'], 500, 500);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 500, 500);
            }

			$json['product'][] = array(
                'id'                            => $product_details['product_id'],
                'name'                          => $product_details['name'],
                'model'                         => $product_details['model'],
                'image'                         => $image,
                'price'                         => $product_details['price'],
                'description'                   => $product_details['description'],
                'attribute_groups'              => $this->model_catalog_product->getProductAttributes($product_details['product_id'])
            );

            echo json_encode($json);
            die;
        } else {
            $this->response->setOutput(json_encode($error));
        }
   }


   public function getCategoriesTree($parent = 0, $level = 1) {
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        
        $result = array();

        $categories = $this->model_catalog_category->getCategories($parent);

        if ($categories && $level > 0) {
            $level--;

            foreach ($categories as $category) {

                $result[] = array(
                    'category_id'   => $category['category_id'],
                    'parent_id'     => $category['parent_id'],
                    'name'          => $category['name'],
                    'categories'    => $this->getCategoriesTree($category['category_id'], $level)
                );
            }

            return $result;
        }
    }


   public function categories()
   { 
      $shop_categories = array();
      $this->load->model('catalog/category');


       $error['fail'] = 'Failed';
        if (isset($this->request->get['json'])) {
            $shop_categories =$this->getCategoriesTree(0, 10);
            echo json_encode($shop_categories);
            die;
        } else {
            $this->response->setOutput(json_encode($error));
        }
   }
}