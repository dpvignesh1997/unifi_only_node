var mongoose = require('mongoose');

var todoSchema = mongoose.Schema({
    todo: { type: String },
    if_done: { type: Boolean, default: false },
    created_at: { type: Date, default: new Date() }
});

module.exports = mongoose.model('totoList', todoSchema);