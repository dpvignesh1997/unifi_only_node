var mongoose = require('mongoose');
Schema = mongoose.Schema;

var postsSchema = mongoose.Schema({
    post_name: { type: String },
    user_ids: [{ type:Schema.Types.ObjectId, ref:'User' }]
});

var Posts = mongoose.model('Posts', postsSchema);
module.exports = Posts;